# dropbox-api

🌐 See the online demo version [here](https://test.johannchopin.fr/dropbox-api/)

## Installation:
1. `npm install` => install all dependencies
2. `npm run dev` => run the app on localhost

## Features:

- Navigate through your folders and files
- Delete files and folders
- Upload a file
- Check our position on a leaflet map