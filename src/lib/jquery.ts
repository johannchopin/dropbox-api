import jquery from "jquery";

export default ((<any>window).$ = (<any>window).jQuery = jquery);