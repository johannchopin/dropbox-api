import fetch = require('isomorphic-fetch');
import { Dropbox } from 'dropbox';
import L from 'leaflet';

import './lib/bootstrap';
import './lib/jquery';

import './styles'

class App {
    private dbx: Dropbox;
    private folderRoute: string[];

    constructor() {
        this.dbx = new Dropbox({ accessToken: '_X7eiEz186AAAAAAAAAAF-iOCRGHvmWxjUB6eXMR03cu6-dGdaDnW3cpHJEMiNfq' });
        this.folderRoute = ['/'];

        document.getElementById('sendFileBtn').onclick = () => this.uploadFile();
        $('#goBack').on('click', () => this.goBackInFolder());

        this.showFolderContent();
        this.initMap();
    }

    private showFolderContent = () => {
        const currentFolder = this.getCurrentFolderPath();
        console.log('current folder: ' + currentFolder);

        this.dbx.filesListFolder({ path: (currentFolder === '/' ? '' : currentFolder) })
            .then((response: Object) => {
                const currentDropboxContent = $('#myDropboxFiles');
                currentDropboxContent.html('');

                response['entries'].forEach(element => {
                    currentDropboxContent.append(this.folderFileRender(element['name'], element['.tag']));
                })

                this.updateFolderRouteBreadcrumb();
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    private updateFolderRouteBreadcrumb = () => {
        const breadcrumb = $('#folderRouteBreadcrumb');
        breadcrumb.html('');

        const folderRoute = this.getFolderRoute();

        folderRoute.forEach((folderName: string) => {
            breadcrumb.append($(`<p>${folderName}</p>`));
        })
    }

    private folderFileRender = (name: string, type: string): JQuery => {
        const folderRender = $('<div class="m-3"></div>');
        let icon: JQuery;
        const deleteIcon = $('<i class="fas fa-times-circle delete-file-btn"></i>');


        switch (type) {
            case 'folder':
                folderRender.addClass('folder')
                folderRender.on('click', () => this.goToFolder('/' + name))
                icon = $(`<i class="fas fa-folder"></i>`).append(deleteIcon);
                break;
            case 'file':
                folderRender.addClass('file')
                icon = $(`<i class="fas fa-file"></i>`).append(deleteIcon);

            default:
                break;
        }

        deleteIcon.on('click', () => this.showAlertModal(
            'Delete ressource',
            'Are you sure that you want to delete the file ' + name,
            'Delete', () => this.deleteFile(name))
        );
        folderRender.append(icon).append($(`<h2>${name}</h2>`));

        return folderRender;
    }

    private uploadFile = (): void => {
        const UPLOAD_FILE_SIZE_LIMIT = 150 * 1024 * 1024;
        const fileInput = document.getElementById('fileUpload');
        // @ts-ignore
        const file = fileInput.files[0];


        if (file.size < UPLOAD_FILE_SIZE_LIMIT) { // File is smaller than 150 Mb - use filesUpload API
            this.dbx.filesUpload({ path: '/' + file.name, contents: file })
                .then((response) => {
                    $('#fileUpload').val("");

                    this.showSuccessModal('File uploaded !')
                    this.showFolderContent();
                })
                .catch(function (error) {
                    console.error(error);
                });
        } else { // File is bigger than 150 Mb - use filesUploadSession* API
            const maxBlob = 8 * 1000 * 1000; // 8Mb - Dropbox JavaScript API suggested max file / chunk size

            var workItems = [];

            var offset = 0;

            while (offset < file.size) {
                var chunkSize = Math.min(maxBlob, file.size - offset);
                workItems.push(file.slice(offset, offset + chunkSize));
                offset += chunkSize;
            }

            const task = workItems.reduce((acc, blob, idx, items) => {
                if (idx == 0) {
                    // Starting multipart upload of file
                    return acc.then(function () {
                        return this.dbx.filesUploadSessionStart({ close: false, contents: blob })
                            .then(response => response.session_id)
                    });
                } else if (idx < items.length - 1) {
                    // Append part to the upload session
                    return acc.then(function (sessionId) {
                        var cursor = { session_id: sessionId, offset: idx * maxBlob };
                        // @ts-ignore
                        return this.dbx.filesUploadSessionAppendV2({ cursor: cursor, close: false, contents: blob }).then(() => sessionId);
                    });
                } else {
                    // Last chunk of data, close session
                    return acc.then(function (sessionId) {
                        var cursor = { session_id: sessionId, offset: file.size - blob.size };
                        var commit = { path: '/' + file.name, mode: 'add', autorename: true, mute: false };
                        // @ts-ignore
                        return this.dbx.filesUploadSessionFinish({ cursor: cursor, commit: commit, contents: blob });
                    });
                }
            }, Promise.resolve());

            task.then(function (result) {
                $('#fileUpload').val("");

                this.showSuccessModal('File uploaded !')
                this.showFolderContent();
            }).catch(function (error) {
                console.error(error);
            });

        }
    }

    private deleteFile = (fileName: string): void => {
        console.log(this.getCurrentFolderPath() + '/' + fileName);
        this.dbx.filesDeleteV2({
            path: this.getCurrentFolderPath() + '/' + fileName
        }).then((response: Object) => {
            this.showSuccessModal('File deleted !')
            this.showFolderContent();
        }).catch(function (error) {
            console.error(error);
        });
    }

    private downloadFile(fileName: string): void {
        this.dbx.filesDownload({
            path: '/' + fileName
        }).then((response: Object) => {
            console.log(response);
        }).catch(function (error) {
            console.error(error);
        });
    }

    private showSuccessModal(msg: string): void {
        $('#modalContent').html(`<div class="alert alert-success" role="alert">${msg}</div>`);

        this.showModal();
    }

    private showModal(): void {
        $('#modal').modal('show')
    }

    private getCurrentFolderName(): string {
        return this.getFolderRoute().slice(-1)[0];
    }

    private goBackInFolder = (): void => {
        const folderRoute = this.getFolderRoute();

        if (folderRoute.length > 1) {
            folderRoute.splice(-1, 1);
            this.folderRoute = folderRoute;

            this.showFolderContent()
        }
    }

    private goToFolder = (path: string) => {
        this.addFolderToFolderRoute(path);

        this.showFolderContent();
    }

    private getFolderRoute = (): string[] => {
        return this.folderRoute.slice();
    }

    private getCurrentFolderPath = (): string => {
        return this.folderRoute.slice(1).join('');
    }

    private addFolderToFolderRoute = (folderName: string): void => {
        this.folderRoute.push(folderName);
    }

    private showAlertModal(title: string, body: string, validateBtnText: string, onOk: () => void): void {
        const validateBtn = $('#alertModal #validate');

        $('#alertModal .modal-title').text(title);
        $('#alertModal .modal-body p').text(body);

        validateBtn.text(validateBtnText);
        validateBtn.on('click', onOk);

        $('#alertModal').modal('show');
    }

    private initMap = (): void => {
        const lat = 48.5978;
        const lon = 6.1449;
        let map = null;

        map = L.map('map').setView([lat, lon], 11);
        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
            minZoom: 1,
            maxZoom: 20
        }).addTo(map);

        L.popup()
            .setLatLng([lat, lon])
            .setContent("Look I'm here")
            .openOn(map);
    }
}

window.onload = function () {
    new App();
}

